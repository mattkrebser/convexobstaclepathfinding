
### Convex Obstacle Path Finding in 2 Dimensions ###

* Shortest Path between two points with convex obstacles.
* Randomly generate convex hulls.
* Uses brute force visibility and AStar graph search to find the minimal path. 

### How do I get set up? ###

* Visual studio 2015 windows form application
* Download repository and build using Visual studio 2015

![img.png](https://bitbucket.org/repo/BrGB9n/images/3599107748-img.png)
