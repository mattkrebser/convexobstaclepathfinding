﻿namespace PathFinder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NextTestButton = new System.Windows.Forms.Button();
            this.NumObstaclesTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // NextTestButton
            // 
            this.NextTestButton.Location = new System.Drawing.Point(537, 516);
            this.NextTestButton.Name = "NextTestButton";
            this.NextTestButton.Size = new System.Drawing.Size(91, 23);
            this.NextTestButton.TabIndex = 0;
            this.NextTestButton.Text = "Next";
            this.NextTestButton.UseVisualStyleBackColor = true;
            this.NextTestButton.Click += new System.EventHandler(this.NextTestButton_Click);
            // 
            // NumObstaclesTextBox
            // 
            this.NumObstaclesTextBox.Location = new System.Drawing.Point(537, 490);
            this.NumObstaclesTextBox.Name = "NumObstaclesTextBox";
            this.NumObstaclesTextBox.Size = new System.Drawing.Size(91, 20);
            this.NumObstaclesTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(534, 474);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Number Obstacles";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(314, 526);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Click two places inside the box to find a path";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 546);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NumObstaclesTextBox);
            this.Controls.Add(this.NextTestButton);
            this.Name = "Form1";
            this.Text = "Convex Obstacle Path Finding";
            this.Click += new System.EventHandler(this.Form1_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button NextTestButton;
        private System.Windows.Forms.TextBox NumObstaclesTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

