﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PathFinder
{
    public partial class Form1 : Form
    {
        Graphics graphics;
        Rectangle bbox;

        /// <summary>
        /// Previously clicked spot
        /// </summary>
        Vector2d PrevPoint = new Vector2d(-10, -10);
        /// <summary>
        /// Most recent clicked spot
        /// </summary>
        Vector2d CurrentPoint = new Vector2d(-10, -10);
        /// <summary>
        /// polygons
        /// </summary>
        List<ConvexPolygon2D> Polygons = new List<ConvexPolygon2D>();

        static Form1 form;
        /// <summary>
        /// Singleton instance
        /// </summary>
        public static Form1 Instance
        {
            get
            {
                if (form == null)
                    form = new Form1();
                return form;
            }
        }

        Form1()
        {
            InitializeComponent();
            graphics = this.CreateGraphics();
            this.Paint += Initialize;
        }
        void Initialize(object sender, PaintEventArgs e)
        {
            bbox = new Rectangle(10, 10, 500, 500);
            Draw();
        }
        /// <summary>
        /// Redraw the graphic.
        /// </summary>
        void Draw()
        {
            //draw surrounding box
            graphics.DrawRectangle(Pens.Black, bbox);
            foreach (var poly in Polygons)
                DrawPolygon(poly, Color.Black);
        }

        /// <summary>
        /// Draw a polygon with the input color
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        public static void DrawPolygon(ConvexPolygon2D p, Color c)
        {
            Pen pen = new Pen(c, 1);
            for (int i = 0; i < p.Vertices.Count; i++)
            {
                if (i + 1 < p.Vertices.Count)
                    Instance.graphics.DrawLine(pen, (System.Drawing.Point)p.Vertices[i],
                        (System.Drawing.Point)p.Vertices[i + 1]);
                else
                    Instance.graphics.DrawLine(pen, (System.Drawing.Point)p.Vertices[i],
                        (System.Drawing.Point)p.Vertices[0]);
            }
        }
        /// <summary>
        /// Draw a line segment
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="c"></param>
        public static void DrawLine(Vector2d p1, Vector2d p2, Color c)
        {
            Pen pen = new Pen(c, 1);
            Instance.graphics.DrawLine(pen, (System.Drawing.Point)p1,
                (System.Drawing.Point)p2);
        }

        /// <summary>
        /// Run another test button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextTestButton_Click(object sender, EventArgs e)
        {
            //parse and validate user input
            int numObstacles = 0;
            Int32.TryParse(NumObstaclesTextBox.Text, out numObstacles);
            numObstacles = numObstacles > 20000 ? 20000 : numObstacles;
            //iteration variables
            int i = 0;
            Random r = new System.Random();
            List<ConvexPolygon2D> NewObstacles = new List<ConvexPolygon2D>();
            List<Rectangle> BBoxes = new List<Rectangle>();
            //generate bboxes
            while (BBoxes.Count < numObstacles && i < 100000)
            {
                int w = r.Next(bbox.Width / 10, bbox.Width / 5 * 2);
                int h = r.Next(bbox.Height / 10, bbox.Height / 5 * 2);
                //generate a random bounds
                int x = r.Next(0, bbox.Width - w);
                int y = r.Next(0, bbox.Height - h);
                Rectangle r1 = new Rectangle(x + bbox.X, y + bbox.Y, w, h);
                //if this bbox does not overlap any existing bboxes
                if (!BBoxes.Any(b => MathI.BBoxIntersects(b.Location, b.Size, r1.Location, r1.Size)))
                {
                    //generate a random polygon to fit inside the bbox
                    var polygon = ConvexPolygon2D.RandomPolygon(r1, 3, 16);
                    NewObstacles.Add(polygon);
                    BBoxes.Add(r1);
                }
                i++;
            }
            Polygons = NewObstacles;
            //redraw 
            graphics.Clear(DefaultBackColor);
            Draw();
            //draw polygons
            foreach (var poly in NewObstacles)
                DrawPolygon(poly, Color.Black);
        }

        /// <summary>
        /// heuristic function for AStar
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        double Heuristic(Vector2d v)
        {
            return Vector2d.Distance(v, CurrentPoint);
        }

        /// <summary>
        /// Every time the form is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Click(object sender, EventArgs e)
        {
            Vector2d new_pos = this.PointToClient(Cursor.Position);
            //make sure points are inbounds
            if (!(new_pos.x > bbox.X && new_pos.y > bbox.Y && new_pos.x < bbox.Right && new_pos.y < bbox.Bottom))
                return;
            //make sure user points aren't inside the polygons.
            if (Polygons.Any(x => x.ContainsPoint(new_pos)))
                return;
            //increment positions
            PrevPoint = CurrentPoint;
            CurrentPoint = new_pos;
            graphics.FillEllipse(Brushes.DarkCyan,
                new Rectangle((int)CurrentPoint.x, (int)CurrentPoint.y, 10, 10));
            //in bounds points
            if (!(PrevPoint.x > 0 && PrevPoint.y > 0 && CurrentPoint.x > 0 && CurrentPoint.y > 0))
                return;
            //add bbox corners
            List<Vector2d> p = new List<Vector2d>();
            p.Add(new Vector2d(bbox.Left, bbox.Bottom));
            p.Add(new Vector2d(bbox.Left, bbox.Top));
            p.Add(new Vector2d(bbox.Right, bbox.Bottom));
            p.Add(new Vector2d(bbox.Right, bbox.Top));
            //add user points
            p.Add(PrevPoint);
            p.Add(CurrentPoint);
            //compute and draw visibility map
            var map = VisibiltyMap.Map(Polygons, p);
            //draw polygons again
            foreach (var poly in Polygons)
                DrawPolygon(poly, Color.Black);
            //do AStar
            var path = map.AStar(PrevPoint, CurrentPoint, Vector2d.Distance, Heuristic);
            //draw path
            for (int j = 0; j < path.Count - 1; j++)
                graphics.DrawLine(new Pen(Color.Green, 2), (System.Drawing.Point)path[j],
                    (System.Drawing.Point)path[j + 1]);
            PrevPoint = new Vector2d(-10, -10);
            CurrentPoint = new Vector2d(-10, -10);
        } 
    }
}
