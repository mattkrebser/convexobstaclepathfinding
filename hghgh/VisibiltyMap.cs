﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinder
{
    public static class VisibiltyMap
    {
        /// <summary>
        /// Compute a graph representing the visibility map for a set of convex polygons and set of points.
        /// </summary>
        /// <param name="Obstacles"></param>
        /// <returns></returns>
        public static HashGraph<Vector2d> Map(IEnumerable<ConvexPolygon2D> Obstacles, 
            IEnumerable<Vector2d> points, bool draw = true)
        {
            // Runtime of O(V * V * E) where V=number of vertices and points, E=number of polygon edges
            //or more simply, O(D * E) where D is all possible diagonals and E is all edges
            HashSet<ConvexPolygon2D.Edge> edges = new HashSet<ConvexPolygon2D.Edge>();
            HashGraph<Vector2d> map = new HashGraph<Vector2d>();
            foreach (var e in AllDiagonals(Obstacles, points))
            {
                if (!edges.Contains(e))
                {
                    edges.Add(e);
                    //count intersections
                    int icount = 0;
                    foreach (var e2 in AllEdges(Obstacles))
                        icount += MathI.LineSegLineSegIntersectionInfo(e.v1, e.v2, e2.v1, e2.v2);
                    //if this diagonal does not intersect any edges, and only intersects 4 or less points
                    if (icount <= 4)
                    {
                        //add it to our graph
                        if (!map.Contains(e.v1))
                            map.Add(e.v1);
                        if (!map.Contains(e.v2))
                            map.Add(e.v2);
                        if (!map.HasEdge(e.v1, e.v2))
                            map.AddEdge(e.v1, e.v2);
                        if (draw)
                            Form1.DrawLine(e.v1, e.v2, System.Drawing.Color.Red);
                    }
                }
            }
            return map;
        }

        /// <summary>
        /// All possible diagonals, may contain duplicates
        /// </summary>
        /// <param name="polygons"></param>
        /// <param name="points"></param>
        /// <returns></returns>
        static IEnumerable<ConvexPolygon2D.Edge> AllDiagonals(IEnumerable<ConvexPolygon2D> polygons, IEnumerable<Vector2d> points)
        {
            //all diagonals from every polygon vertice to every polygon vertice
            foreach (var p1 in polygons)
                foreach (var v1 in p1.Vertices)
                    foreach (var p2 in polygons)
                        if (p1 != p2)
                            foreach (var v2 in p2.Vertices)
                                yield return new ConvexPolygon2D.Edge(v1, v2);
            //all lines from all points to all points
            foreach (var v1 in points)
                foreach (var v2 in points)
                    if (v1 != v2)
                        yield return new ConvexPolygon2D.Edge(v1, v2);
            //all lines from all points to all polygon vertices
            foreach (var p1 in polygons)
                foreach (var v1 in p1.Vertices)
                    foreach (var v2 in points)
                        yield return new ConvexPolygon2D.Edge(v1, v2);
            //all polygon edges
            foreach (var p1 in polygons)
                foreach (var e in p1.Edges)
                    yield return e;
            
        }
        static IEnumerable<ConvexPolygon2D.Edge> AllEdges(IEnumerable<ConvexPolygon2D> polygons)
        {
            foreach (var p in polygons)
                foreach (var e in p.Edges)
                    yield return e;
        }
    }
}
