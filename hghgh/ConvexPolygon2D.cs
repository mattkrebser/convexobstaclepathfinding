﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace PathFinder
{
    public class ConvexPolygon2D
    {
        /// <summary>
        /// Ordered list of points for the ConvexPolygon2D.
        /// </summary>
        public List<Vector2d> Vertices;

        /// <summary>
        /// All edges in this polygon
        /// </summary>
        public IEnumerable<Edge> Edges
        {
            get
            {
                if (Vertices == null || Vertices.Count < 2) yield break;
                for (int i = 0; i + 1 < Vertices.Count; i++)
                    yield return new Edge(Vertices[i], Vertices[i + 1]);
                yield return new Edge(Vertices[Vertices.Count - 1], Vertices[0]);
            }
        }

        /// <summary>
        /// Make a new convex polygon with the input set of ordered point. 
        /// Points are ordered as follows: v0v1 = edge0, v1v2 = edge1, v2v3 = edge2,...vn-1vn = edgen-1
        /// </summary>
        /// <param name="Verts"></param>
        public ConvexPolygon2D(List<Vector2d> Verts)
        {
            if (Verts == null || Verts.Count < 3)
                throw new System.Exception("Error, insufficient vertices");
            Vertices = Verts;
        }

        static System.Random randGen;
        /// <summary>
        /// Returns a new Randomized ConvexPolygon
        /// </summary>
        /// <param name="minPoints"></param>
        /// <param name="maxPoints"></param>
        /// <returns></returns>
        public static ConvexPolygon2D RandomPolygon(System.Drawing.Rectangle bounds,
            int minPoints = 3, int maxPoints = 32)
        {
            if (randGen == null)
                randGen = new Random();
            if (maxPoints < minPoints || minPoints < 3 || maxPoints < 3)
                throw new System.Exception("Invalid number of points range");

            maxPoints = randGen.Next(minPoints, maxPoints);

            //generate random points
            List<Vector2d> pointList = new List<Vector2d>();
            for (int i = 0; i < maxPoints; i++)
            {
                Vector2d point = new Vector2d(randGen.NextDouble() * bounds.Width + bounds.X,
                    randGen.NextDouble() * bounds.Height + bounds.Y);
                pointList.Add(point);
            }

            var l = pointList.Select(x => (Point)x).ToList();
            return new ConvexPolygon2D(GrahamScan.convexHull(l).Select(x => (Vector2d)x).ToList());
        }

        /// <summary>
        /// Returns true if this ConvexPolygon contains the input point inside of it
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool ContainsPoint(Vector2d p)
        {
            foreach (var e in Edges)
            {
                if (!MathI.Left(p, e.v1, e.v2))
                    return false;
            }
            return true;
        }

        struct Tuple<T, T1>
        {
            public T v1;
            public T1 v2;
            public Tuple(T t, T1 t1)
            {
                v1 = t; v2 = t1;
            }
        }

        public override string ToString()
        {
            string s = "ConvexPolygon:{";
            if (Vertices != null)
                for (int i = 0; i < Vertices.Count; i++)
                {
                    s += Vertices[i].ToStringShort() + ",";
                }
            s += "}";
            return s;
        }

        /// <summary>
        /// Structure representing an edge
        /// </summary>
        public struct Edge : IEquatable<Edge>
        {
            public Vector2d v1;
            public Vector2d v2;
            public Edge(Vector2d a, Vector2d b)
            {
                v1 = a; v2 = b;
            }
            public override bool Equals(object obj)
            {
                return obj is Edge ? Equals((Edge)obj) : false;
            }
            public bool Equals(Edge e)
            {
                return (v1 == e.v1 && v2 == e.v2) || (v1 == e.v2 && v2 == e.v1);
            }
            public override int GetHashCode()
            {
                unchecked
                {
                    int hash = 29;
                    hash = 17 * hash + v1.GetHashCode();
                    hash = 17 * hash + v2.GetHashCode();
                    return hash;
                }
            }
        }
    }
}
